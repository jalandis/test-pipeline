<?php

namespace ImageFirst\Tracs\Queries;

use Doctrine\DBAL\DriverManager;
use PHPUnit\Framework\TestCase;

/**
 * Tests new queries (requires database configuration).
 */
final class QueryTest extends TestCase
{
    /**
     * Test that schemas do not contain empty tables.
     *
     * If there are legitimate purposes for these tables, exceptions should be
     * documented and filtered with a NOT IN clause.
     *
     * In DEV databases (tracs, tracs_common, tracs_data4, and tracs_dataE) all
     * contain 352 empty tables.  This is over 30 percent of all tables in each
     * schema.
     *
     * In particular, invoicing is made a great deal more complicated due to
     * empty archiveh and archived tables.  Are they being used anywhere and for
     * what purpose?
     *
     * @requires extension mysqli
     */
    public function testNoEmptyTables()
    {
        $configuration = require __DIR__ . '/../../../config/tracs.php';
        $conn = DriverManager::getConnection($configuration['common']);

        $databases = $conn->fetchAll("SELECT dbname FROM tracsdatabases", []);
        $databases[] = ['dbname' => 'tracs_common'];
        foreach ($databases as $database) {
            try {
                $configuration['common']['dbname'] = $database['dbname'];
                $conn = DriverManager::getConnection($configuration['common']);
                $conn->fetchAll('SELECT 1 FROM DUAL', []);
            } catch (\Exception $exception) {
                continue;
            }

            $missingTablesCount = count($conn->fetchAll(
                'SELECT table_name
                 FROM information_schema.tables
                 WHERE table_rows = 0',
                []
            ));

            $this->assertTrue(
                $missingTablesCount === 0,
                "Found ${missingTablesCount} empty tables in '${database['dbname']}'"
            );
        }
    }

    /**
     * Tests that re-written invoice query produces the same results faster.
     *
     * Adding index: ALTER TABLE activity ADD INDEX invoicing (type, description(45))
     * Running scenario with/without index shows a 100-150% improvement in time.
     * 45 was choosen due to the standard 36 and 42 string length of descriptions
     * used in all queries.
     *
     * @todo This scenario is time consuming and can be disabled/removed after
     *       changes are implemented.
     *
     * @requires extension mysqli
     */
    public function testInvoiceQuery()
    {
        $oldQueries = [
            "SELECT *
             FROM armaster
             WHERE autostmoptin = 'Y'
             AND custnum NOT LIKE '%/%'
             AND company = 1",
            "SELECT custnum
             FROM activity a
             INNER JOIN custactivities c ON c.activityID = a.activityID
             WHERE c.custnum = ?
             AND description = ?
             AND type = 'MINV'
             LIMIT 1",
            "SELECT custnum
             FROM activity a
             INNER JOIN custactivities c ON c.activityID = a.activityID
             WHERE c.custnum = ?
             AND description = ?
             AND (type = 'REM' OR type = 'ASMT')
             LIMIT 1"
        ];

        $newQuery = "SELECT DISTINCT armaster.*
                     FROM armaster
                     WHERE autostmoptin = 'Y'
                     AND custnum not LIKE '%/%'
                     AND company = 1
                     AND NOT EXISTS (
                         SELECT NULL
                         FROM activity
                         INNER JOIN custactivities USING (activityID)
                         WHERE custnum = armaster.custnum
                         AND ((
                             armaster.statementcode = 'I'
                             AND type = 'MINV'
                             AND description = ?
                         ) OR (
                             COALESCE(armaster.statementcode, '') != 'I'
                             AND type IN ('REM', 'ASMT')
                             AND description = ?
                         ))
                     )";

        $configuration = require __DIR__ . '/../../../config/tracs.php';
        $conn = DriverManager::getConnection($configuration['common']);

        $databases = $conn->fetchAll(
            "SELECT *
             FROM tracsdatabases
             WHERE dbname NOT IN ('tracs')",
            []
        );

        $year = 2014;
        while ($year < 2020) {
            $month = 1;
            while ($month < 13) {
                $totalOldTime = 0;
                $totalNewTime = 0;
                foreach ($databases as $database) {

                    /**
                     * Connect to new database and execute a smoke test.
                     *
                     * Skips missing databases which is a common situation in DEV.
                     */
                    try {
                        $configuration['common']['dbname'] = $database['dbname'];
                        $conn = DriverManager::getConnection($configuration['common']);
                        $conn->fetchAll('SELECT 1 FROM DUAL', []);
                    } catch (\Exception $exception) {
                        continue;
                    }

                    $date = new \DateTime("${year}-${month}-01");
                    $endOfMonth = $date->format('Y-m-t');
                    $monthlyDescription = "Monthly Invoice ending ${endOfMonth} emailed";
                    $statementDescription = "Statement ending ${endOfMonth} emailed";

                    // Approximation of old method.
                    $expected = 0;
                    $timeStart = microtime(true);
                    $unfiltered = $conn->fetchAll($oldQueries[0], []);
                    foreach ($unfiltered as $rawRecord) {
                        if ($rawRecord['statementcode'] === 'I') {
                            $sent = $conn->fetchAll($oldQueries[1], [
                                $rawRecord['custnum'],
                                $monthlyDescription
                            ]);
                        } else {
                            $sent = $conn->fetchAll($oldQueries[2], [
                                $rawRecord['custnum'],
                                $statementDescription
                            ]);
                        }

                        if (count($sent) === 0) {
                            $expected++;
                        }
                    }
                    $oldTime = microtime(true) - $timeStart;
                    $totalOldTime += $oldTime;

                    // New unified query.
                    $timeStart = microtime(true);
                    $found = count($conn->fetchAll($newQuery, [
                        $monthlyDescription,
                        $statementDescription
                    ]));
                    $newTime = microtime(true) - $timeStart;
                    $totalNewTime += $newTime;

                    $this->assertEquals(
                        $expected,
                        $found,
                        "New Query produces different results : '${database['dbname']}' ${endOfMonth}"
                    );

                    $message = "New Query runs slower: '${database['dbname']}' ${endOfMonth}" . PHP_EOL;
                    $message .= "Results returned: ${expected}" . PHP_EOL;
                    $message .= "Old time: ${oldTime}, New time: ${newTime}" . PHP_EOL;

                    /**
                     * Assert there is a performance improvement.
                     *
                     * Increase in time noted when small number of results returned (1 or 2).
                     * Increases noted to be less than 1E-3 on virtual machine.
                     * Ignoring this case as it will not have a real impact on the process.
                     */
                    $this->assertTrue(
                        $oldTime > $newTime || ($expected < 2 && abs($oldTime - $newTime) < 1E-3),
                        $message
                    );
                }

                /**
                 * Seeing around 60% improvement but actual time saved seems
                 * insignificant :-(
                 *
                 * Expect in PRD the increased number of databases and results
                 * will improve the impact but not enough to justify more effort
                 * spent on this right now.
                 */
                $totalImprovement = ($totalNewTime / $totalOldTime) * 100;
                echo PHP_EOL;
                echo "Total Improvement over all databases: ${totalImprovement}%" . PHP_EOL;
                echo "Old time: ${totalOldTime}" . PHP_EOL;
                echo "New time: ${totalNewTime}" . PHP_EOL;
                $month++;
            }
            $year++;
        }
    }
}
