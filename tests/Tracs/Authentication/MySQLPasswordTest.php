<?php

namespace ImageFirst\Tracs\Authentication;

use Doctrine\DBAL\DriverManager;
use PHPUnit\Framework\TestCase;

final class MySQLPasswordTest extends TestCase
{
    private $conn;

    /**
     * Setup database connection for tests.
     */
    protected function setup()
    {
        $configuration = require __DIR__ . '/../../../config/tracs.php';
        $this->conn = DriverManager::getConnection($configuration['common']);
    }

    /**
     * Generates a random string for testing.
     *
     * @param  int    $length Length of string
     * @return string         Random string
     */
    private function randomString(int $length)
    {
        $alphabet = '\'",.?<>!@#$%^&*()_+-=[]\{}|0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $alphabetLength = strlen($alphabet) - 1;

        $result = '';
        for ($index = 0; $index < $length; $index++) {
            $result .= $alphabet[rand(0, $alphabetLength)];
        }
        return $result;
    }

    /**
     * Test MySQL OLD_PASSWORD hash against PHP implementation.
     *
     * @requires extension mysqli
     */
    public function testOldPasswordHash()
    {
        for ($index = 0; $index < 100; $index++) {
            $test = $this->randomString(rand(1, 255));
            $expected = $this->conn->fetchAssoc(
                'SELECT OLD_PASSWORD(?) as password FROM dual',
                [$test]
            );
            $this->assertEquals(
                $expected['password'],
                MySQLPassword::old_password($test),
                'Found difference between MySQL OLD_PASSWORD hashes'
            );
        }
    }
}
