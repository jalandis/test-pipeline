<?php

namespace ImageFirst\Tracs\Authentication;

use Doctrine\DBAL\Connection;
use PHPUnit\Framework\TestCase;

final class PasswordPolicyTest extends TestCase
{

    /**
     * Tests isPasswordResetRequired.
     */
    public function testIsPasswordResetRequired()
    {
        $user = ['upassdate' => '2000-01-01'];
        $errorMessage = 'An old password should require a reset';
        $this->assertTrue(
            PasswordPolicy::isPasswordResetRequired($user),
            $errorMessage
      );

        $user = ['upassdate' => null];
        $errorMessage = 'Password should require reset if upassdate is null';
        $this->assertTrue(
            PasswordPolicy::isPasswordResetRequired($user),
            $errorMessage
      );

        $user = ['upassdate' => ''];
        $errorMessage = 'Password should require reset if upassdate is empty string';
        $this->assertTrue(
            PasswordPolicy::isPasswordResetRequired($user),
            $errorMessage
      );

        $user = [];
        $errorMessage = 'Password should require reset if upassdate is undefined';
        $this->assertTrue(
            PasswordPolicy::isPasswordResetRequired($user),
            $errorMessage
      );
    }

    /**
     * Tests password strength.
     */
    public function testValidatePasswordStrength()
    {
        $user = [
        'uname' => 'testing',
        'upass' => '3d39fcec1d031f94',
        'priorupass1' => '04e0df0824dd66fb',
        'priorupass2' => '$2y$10$8fYjnBELXcyBE16c1VYdIe2ljpa27MbjkIHVypDQD7Q3JSr7JLTtW',
        'priorupass3' => '---'
      ];

        $errorMessage = 'Password should be valid';
        $this->assertEquals(
            PasswordPolicy::VALID_PASSWORD,
            PasswordPolicy::validatePasswordStrength($user, 'aB-123teng'),
            $errorMessage
      );

        $errorMessage = 'Password should be valid with 3 of 4 required character sets';
        $this->assertEquals(
            PasswordPolicy::VALID_PASSWORD,
            PasswordPolicy::validatePasswordStrength($user, 'a-123teng'),
            $errorMessage
      );

        $errorMessage = 'Password is required to be greater than 6 characters';
        $this->assertEquals(
            PasswordPolicy::TOO_SMALL_ERROR,
            PasswordPolicy::validatePasswordStrength($user, 'aB-12'),
            $errorMessage
      );

        $errorMessage = 'Password needs either uppercase or lowercase letters';
        $this->assertEquals(
            PasswordPolicy::MISSING_CHARACTER_SET_ERROR,
            PasswordPolicy::validatePasswordStrength($user, '-12345'),
            $errorMessage
      );

        $errorMessage = 'Password needs either numbers or uppercase letters';
        $this->assertEquals(
            PasswordPolicy::MISSING_CHARACTER_SET_ERROR,
            PasswordPolicy::validatePasswordStrength($user, 'abcde-'),
            $errorMessage
      );

        $errorMessage = 'Password needs either numbers or lowercase letters';
        $this->assertEquals(
            PasswordPolicy::MISSING_CHARACTER_SET_ERROR,
            PasswordPolicy::validatePasswordStrength($user, '-ABCDE'),
            $errorMessage
      );

        $errorMessage = 'Password needs non-alphanumeric character or uppercase letters';
        $this->assertEquals(
            PasswordPolicy::MISSING_CHARACTER_SET_ERROR,
            PasswordPolicy::validatePasswordStrength($user, 'ab1234'),
            $errorMessage
      );

        $errorMessage = 'Password cannot start with part of username greater than 2';
        $this->assertEquals(
            PasswordPolicy::CONTAINS_USERNAME_ERROR,
            PasswordPolicy::validatePasswordStrength($user, 'tes12-A'),
            $errorMessage
      );

        $errorMessage = 'Password cannot end with part of username greater than 2';
        $this->assertEquals(
            PasswordPolicy::CONTAINS_USERNAME_ERROR,
            PasswordPolicy::validatePasswordStrength($user, 'ing12-A'),
            $errorMessage
      );

        $errorMessage = 'Password cannot match current password';
        $this->assertEquals(
            PasswordPolicy::MATCHES_PREVIOUS_ERROR,
            PasswordPolicy::validatePasswordStrength($user, 'Password123-'),
            $errorMessage
      );

        $errorMessage = 'Password cannot match any of last 3 passwords (MySQL hash)';
        $this->assertEquals(
            PasswordPolicy::MATCHES_PREVIOUS_ERROR,
            PasswordPolicy::validatePasswordStrength($user, 'Password1234-'),
            $errorMessage
      );

        $errorMessage = 'Password cannot match any of last 3 passwords (Bcrypt hash)';
        $this->assertEquals(
            PasswordPolicy::MATCHES_PREVIOUS_ERROR,
            PasswordPolicy::validatePasswordStrength($user, 'Password12345-'),
            $errorMessage
      );
    }
}
