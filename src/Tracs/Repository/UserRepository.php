<?php

namespace ImageFirst\Tracs\Repository;

use Doctrine\DBAL\Connection;

/**
 * Repository for all User database queries.
 */
final class UserRepository
{
    private $conn;

    public function __construct(Connection $conn)
    {
        $this->conn = $conn;
    }

    /**
     * Returns a user record for a unique username.
     *
     * @param  string      $username Unique username
     * @return array|bool            User record or false
     */
    public function getUserByName(string $username)
    {
        return $this->conn->fetchAssoc(
            'SELECT * FROM users WHERE uname = ?',
            [$username]
        );
    }

    /**
     * Records a users successful login attempt.
     *
     * @param  string  $username Unique username
     * @return bool              Boolean indicating success
     */
    public function recordSuccessfulLogin(string $username) : bool
    {
        return $this->conn->executeUpdate(
            'UPDATE users
             SET failedlogins = 0,
                 failedsecurityquestion = 0
             WHERE uname = ?',
            [$username]
        ) === 1;
    }

    /**
     * Records users failed login attempt.
     *
     * @param  string  $username Unique username
     * @return bool              Boolean indicating success
     */
    public function recordFailedLogin(string $username) : bool
    {
        return $this->conn->executeUpdate(
            'UPDATE users
             SET failedlogins = failedlogins + 1
             WHERE uname = ?',
            [$username]
        ) === 1;
    }


    /**
     * Records users failed security question attempt.
     *
     * @param  string  $username Unique username
     * @return bool              Boolean indicating success
     */
    public function recordFailedSecurityQuestion(string $username) : bool
    {
        return $this->conn->executeUpdate(
            'UPDATE users
             SET failedsecurityquestion = failedsecurityquestion + 1
             WHERE uname = ?',
            [$username]
        ) === 1;
    }

    /**
     * Updates a users password.
     *
     * Passwords are hashed with PHP Bcrypt method.
     * A record of 3 prior passwords are kept to prevent password reuse.
     * A users failed login and security question counts are also reset to 0.
     *
     * @param  string  $username Unique username
     * @param  string  $password Users new password
     * @return bool              Boolean indicating record updated
     */
    public function updateUsersPassword(string $username, string $password) : bool
    {
        $passwordHash = password_hash($password, PASSWORD_DEFAULT);

        return $this->conn->executeUpdate(
            'UPDATE users u1
             INNER JOIN users u2 USING (uid)
             SET u1.upass = ?,
                 u1.upassdate = ?,
                 u1.failedlogins = 0,
                 u1.failedsecurityquestion = 0,
                 u1.priorupass1 = u2.upass,
                 u1.priorupass2 = u2.priorupass1,
                 u1.priorupass3 = u2.priorupass2
             WHERE u1.uname = ?',
            [$passwordHash, Date('Y-m-d'), $username]
        ) === 1;
    }

    /**
     * Forces a user to reset password on next login.
     *
     * @param  string  $username Unique username
     * @return bool              Boolean indicating record updated
     */
    public function forcePasswordReset(string $username) : bool
    {
        return $this->conn->executeUpdate(
            'UPDATE users SET upassdate = ? WHERE uname = ?',
            ['', $username]
        ) === 1;
    }

    /**
     * Updates a users security question.
     *
     * @param  string  $username Unique username
     * @param  string  $question Users security question
     * @param  string  $answer   Users security answer
     * @return bool              Boolean indicating success
     */
    public function updateUsersSecurityQuestion(
        string $username,
        string $question,
        string $answer
    ) : bool {
        return $this->conn->executeUpdate(
            'UPDATE users
             SET secureq = ?,
                 securea = ?
             WHERE uname = ?',
            [$question, $answer, $username]
        ) === 1;
    }

    /**
     * Records any action a user takes in the userlog table.
     *
     * @param  string  $username       Unique username
     * @param  string  $action         Action to record
     * @param  string  $program        Page recording action
     * @param  string  $useragent      Useragent associated with action
     * @param  string  $destinationurl Optional destination url associated with action
     * @return bool                    Boolean indicating success
     */
    public function logUserAction(
        string $username,
        string $action,
        string $program,
        string $useragent = '',
        string $destinationurl = ''
    ) : bool {

        // Default user agent setup.
        if (empty($useragent)) {
            $useragent = $_SERVER['HTTP_USER_AGENT'];
        }

        $query = 'INSERT INTO userlog
                  SET transdate = ?,
                      uname = ?,
                      action = ?,
                      remoteaddr = ?,
                      port = ?,
                      useragent = ?,
                      program = ?,
                      destinationurl = ?';
        return $this->conn->executeQuery($query, [
            date('Y-m-d H:i:s'),
            $username,
            $action,
            $_SERVER['REMOTE_ADDR'],
            $_SERVER['SERVER_PORT'],
            $useragent,
            $program,
            $destinationurl
        ])->rowCount() === 1;
    }
}
