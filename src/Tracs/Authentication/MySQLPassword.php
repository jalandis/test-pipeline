<?php

namespace ImageFirst\Tracs\Authentication;

/**
 * Class handles deprecated MySQL hashing algorithm.
 */
final class MySQLPassword
{

    /**
     * Replicates MySQL old PASSWORD() hash function.
     *
     * @see https://fastapi.metacpan.org/source/IKEBE/Crypt-MySQL-0.04/lib/Crypt/MySQL.xs
     *      Perl C MySQL Algorithm
     * @param  string  $password Password to be hashed
     * @return string            Generated hash
     */
    public static function old_password(string $password) : string
    {
        $len = strlen($password);
        $add = 7;
        $nr  = 1345345333;
        $nr2 = 0x12345671;
        $tmp = 0;

        foreach (str_split($password) as $chr) {
            if ($chr == " " || $chr == "\t") {
                continue;
            }

            $tmp  = ord($chr);
            $nr  ^= ((($nr & 0x3f) + $add) * $tmp) + ($nr << 8);
            $nr2 += ($nr2 << 8) ^ $nr;
            $nr2 &= 0xffffffff;
            $add += $tmp;
        }

        $nr &= 0x7fffffff;
        $nr2 &= 0x7fffffff;
        return sprintf("%08x%08x", $nr, $nr2);
    }
}
