<?php

namespace ImageFirst\Tracs\Authentication;

/**
 * Class defines and validates password policy rules.
 */
final class PasswordPolicy
{

    /**
     * Result codes for password validation.
     */
    const VALID_PASSWORD = 0;
    const TOO_SMALL_ERROR = 1;
    const MISSING_CHARACTER_SET_ERROR = 2;
    const CONTAINS_USERNAME_ERROR = 3;
    const MATCHES_PREVIOUS_ERROR = 4;

    /**
     * Password policy constraints.
     */
    const MAX_LOGIN_ATTEMPTS = 10;
    const MAX_SECURITY_QUESTION_ATTEMPTS = 3;
    const MAX_PASSWORD_AGE = 120;
    const MINIMUM_LENGTH = 6;
    const MAX_USERNAME_SUBSTRING = 2;
    const REQUIRED_CHARACTER_SETS_COUNT = 3;
    const POSSIBLE_CHARACTER_SETS = [
        '@[a-z]@',
        '@[A-Z]@',
        '@[0-9]@',
        '@[^a-zA-Z0-9]@'
    ];

    /**
     * Validates a new passwords strength.
     *
     * Rules:
     * - Password must be a minimum length.
     * - Password must contains a number of required character sets.
     * - Password cannot contain subset of username above a threshold.
     * - Password cannot be the same as the current or past 3 passwords.
     *
     * @param  array   $user     User record
     * @param  string  $password Possible new password
     * @return int               Error code (see class constants)
     */
    public static function validatePasswordStrength(array $user, string $password) : int
    {
        if (strlen($password) < self::MINIMUM_LENGTH) {
            return self::TOO_SMALL_ERROR;
        }

        $foundSets = 0;
        foreach (self::POSSIBLE_CHARACTER_SETS as $set) {
            if (preg_match($set, $password)) {
                $foundSets++;
            }
        }

        if ($foundSets < self::REQUIRED_CHARACTER_SETS_COUNT) {
            return self::MISSING_CHARACTER_SET_ERROR;
        }

        for ($index = 0; $index < strlen($password) - self::MAX_USERNAME_SUBSTRING; $index++) {
            $peice = substr($password, $index, self::MAX_USERNAME_SUBSTRING + 1);
            if (strpos($user['uname'], $peice) !== false) {
                return self::CONTAINS_USERNAME_ERROR;
            }
        }

        if (self::validatePassword($user['upass'], $password)
            || self::validatePassword($user['priorupass1'], $password)
            || self::validatePassword($user['priorupass2'], $password)
            || self::validatePassword($user['priorupass3'], $password)
        ) {
            return self::MATCHES_PREVIOUS_ERROR;
        }

        return self::VALID_PASSWORD;
    }

    /**
     * Validates a users password.
     *
     * This method is meant as a temporary wrapper for password_verify and
     * should be removed when insecure MySQL password storage is ended.
     *
     * Hashing algorithms checked:
     * - PHP Bcrypt
     * - MySQL OLD_PASSWORD
     *
     * @param  string  $hash     Password hash
     * @param  string  $password Password to validate
     * @return bool              Boolean indicating if password is valid
     */
    public static function validatePassword(string $hash, string $password) : bool
    {
        return password_verify($password, $hash) ||
            $hash == MySQLPassword::old_password($password);
    }

    /**
     * Tests if password reset is required.
     *
     * Rules:
     * - Password must be updated every n days.
     * - No recorded date means the password requires a reset
     *
     * @param  array  $user User database record.
     * @return bool         True if password reset is required
     */
    public static function isPasswordResetRequired(array $user) : bool
    {
        if (!isset($user['upassdate']) || empty($user['upassdate'])) {
            return true;
        }

        $now = new \DateTime();
        $lastUpdated = new \DateTime($user['upassdate']);
        return $now->diff($lastUpdated)->format('%a') > self::MAX_PASSWORD_AGE;
    }

    /**
     * Tests if account login is locked.
     *
     * @param  array  $user User database record.
     * @return bool         True if account login is locked
     */
    public static function isAccountLocked(array $user) : bool
    {
        return $user["failedlogins"] >= self::MAX_LOGIN_ATTEMPTS;
    }

    /**
     * Tests if account security question is disabled.
     *
     * @param  array  $user User database record.
     * @return bool         True if account security question is disabled
     */
    public static function isSecurityQuestionDisabled(array $user) : bool
    {
        return $user["failedsecurityquestion"] >= self::MAX_SECURITY_QUESTION_ATTEMPTS;
    }

    /**
     * Creates a random 6 character password.
     *
     * @return string A random password
     */
    public static function generateRandomPassword() : string
    {
        $pass = '';

        // One upper case character.
        $pass .= chr(random_int(65, 90));

        // One numeric character.
        $pass .= chr(random_int(48, 57));

        // One non-alphanumeric character.
        $pass .= chr(random_int(33, 47));

        // 3 lower case characters.
        $pass .= chr(random_int(97, 122));
        $pass .= chr(random_int(97, 122));
        $pass .= chr(random_int(97, 122));

        return str_shuffle($pass);
    }
}
